//Теория:
//Метод объекта - это функция внутри самого объекта. Мы можем вызывать метод объекта, также как и его свойство, только в конце нужно добавить скобки. Также,существует множество встроенных методов, позволяющие выполнять те, или иные функции.

//Задание:
let newUser;
function createNewUser(){
    newUser = {};
    let name = prompt('Enter your name:');
    let surname = prompt('Enter you surname:');
    newUser.firstName = name;
    newUser.lastName = surname;
    newUser.getLogin = function() {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    };
    return newUser;
}
console.log( createNewUser() );

Object.defineProperty(newUser, 'firstName', {
    writable: false,
});
Object.defineProperty(newUser, 'lastName', {
    writable: false,
});

console.log( newUser.getLogin() );

//С сеттерами сложно...
